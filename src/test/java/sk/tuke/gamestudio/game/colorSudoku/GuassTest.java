package sk.tuke.gamestudio.game.colorSudoku;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import sk.tuke.gamestudio.game.colorSudoku.core.Guess;

import java.util.Random;

public class GuassTest {

    private Random random = new Random();
    private int correctValue;
    private Guess<Integer> cell;

    public GuassTest() {
        correctValue = random.nextInt();
        cell = new Guess<>(correctValue);
    }

    @Test
    public void checkSetGuessValue() {
        int test_value = random.nextInt();
        cell.setValue(test_value);
        assertEquals(test_value, cell.getValue(), "Unexpected value after setValue.");
    }

    @Test
    public void checkRemoveValue() {
        cell.removeValue();
        assertEquals(null, cell.getValue(), "Cell should not contain any value");
    }

}
