package sk.tuke.gamestudio.game.colorSudoku;

import org.junit.jupiter.api.Test;
import sk.tuke.gamestudio.game.colorSudoku.core.Color;

import static org.junit.jupiter.api.Assertions.*;

public class ColorTest {

    @Test
    public void checkNumberOfCollors() {
        assertTrue(9 <= Color.values().length,
                "Color should have at least 9 distinct colors for classic sudoku game");
    }
}
