package sk.tuke.gamestudio.game.colorSudoku;

import org.junit.jupiter.api.Test;
import sk.tuke.gamestudio.game.colorSudoku.core.OptionQueue;

import static org.junit.jupiter.api.Assertions.*;

import java.util.NoSuchElementException;
import java.util.Random;

public class OptionQueueTest {

    private Random random = new Random();
    private OptionQueue queue;
    private int x;
    private int y;

    public OptionQueueTest() {
        x = random.nextInt();
        y = random.nextInt();
        queue = new OptionQueue(x, y);
    }

    @Test
    public void checkX() {
        assertEquals(x, queue.getX(), "X has been initialized incorrectly.");
    }

    @Test
    public void checkY() {
        assertEquals(y, queue.getY(), "X has been initialized incorrectly.");
    }

    @Test
    public void checkEmptyDeque() {
//        assertEquals((Double) null, queue.dequeue(), "Empty deque did not return null.");
        assertThrows(NoSuchElementException.class, () -> queue.dequeue(),
                "OptionQueue did not throw exception when dequeue and empty queue");
    }

    @Test
    public void checkNonEmptyDequeue() {
        while (queue.size() > 0) {
            queue.dequeue();
        }
        int element = random.nextInt();
        queue.equeue(element);
        assertEquals(element, queue.dequeue(),
                "Unexpected element after dequeue non empty queue.");
        assertEquals(0, queue.size(), "Queue should have size 0 after dequeue.");
    }

}
