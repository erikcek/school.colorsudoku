package sk.tuke.gamestudio.game.colorSudoku;

import org.junit.jupiter.api.Test;
import sk.tuke.gamestudio.game.colorSudoku.core.SudokuSolver;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.Random;
import java.util.stream.Stream;

public class SudokuSolverTest {

    private Random random = new Random();
    private SudokuSolver solver;

    @Test
    public void checkSolve9x9Sudoku() {
        try {
            int[][] riddle = new int[9][9];
            URL fileUrl = getClass().getClassLoader().getResource("sudoku.csv");
            File sudoku = new File(fileUrl.getFile());
//            Stream<String> lines = ClassLoader.getSystemResourceAsStream("sudoku.csv");
            Stream<String> lines =  Files.lines(sudoku.toPath());
            int linesToSkip = random.nextInt(65530);
            String riddleWithSolution = lines.skip(linesToSkip).findFirst().get();
            String sudokuRiddle = riddleWithSolution.split(",")[0];
            String sudokuSolution = riddleWithSolution.split(",")[1];

            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    riddle[i][j] = Integer.parseInt(sudokuRiddle.substring(i*9 + j, i*9 + j + 1));
                }
            }
            solver = new SudokuSolver(9, 3, riddle);
            int[][] result = solver.solveRiddle();

            String resultString = "";
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    resultString += result[i][j];
                }
            }

            assertEquals(sudokuSolution, resultString, "Sudoku solver returned incorrect result.");
        }
        catch (IOException e) {
            assertTrue(false, "Unable to load sudoku riddles and solutions.");
        }
    }
}
