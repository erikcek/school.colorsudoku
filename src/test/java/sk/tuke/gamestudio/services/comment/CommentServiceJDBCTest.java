package sk.tuke.gamestudio.services.comment;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import sk.tuke.gamestudio.entity.Comment;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommentServiceJDBCTest {

    private Comment comment;

    @Test
    public void addCommentTest() throws CommentException {
        CommentService service = new CommentServiceJDBC();
        comment = new Comment("MaxScorePlayer","test_game", "Test comment", new Date());
        service.addComment(comment);
        List<Comment> comments = service.getComments("test_game");

        assertEquals(1, comments.size());
        assertEquals("MaxScorePlayer", comments.get(0).getPlayer());
        assertEquals("Test comment", comments.get(0).getComment());
        assertEquals("test_game", comments.get(0).getGame());
    }

    @AfterEach
    public void removeComment() throws CommentException {
        CommentServiceJDBC service = new CommentServiceJDBC();
        service.removeComment(comment);
        List<Comment> comments = service.getComments(comment.getGame());
        assertEquals(0, comments.size());
    }
}
