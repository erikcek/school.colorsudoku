package sk.tuke.gamestudio.services.score;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import sk.tuke.gamestudio.entity.Score;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;
import java.util.List;

public class ScoreServiceJDBCTest {

    private Score score;

    @Test
    public void addScoreTest() {
        ScoreService service = new ScoreServiceJDBC();
        score = new Score("test_game","MaxScorePlayer", 3000, new Date());
        service.addScore(score);
        List<Score> scores = service.getTopScores("test_game");

        assertEquals(1, scores.size());
        assertEquals("MaxScorePlayer", scores.get(0).getPlayer());
        assertEquals(3000, scores.get(0).getPoints());
        assertEquals("test_game", scores.get(0).getGame());
    }

    @AfterEach
    public void cleanScore() {
        ScoreServiceJDBC service = new ScoreServiceJDBC();
        service.removeScore(score);
        List<Score> scores = service.getTopScores(score.getGame());
        assertEquals(0, scores.size());
    }

}
