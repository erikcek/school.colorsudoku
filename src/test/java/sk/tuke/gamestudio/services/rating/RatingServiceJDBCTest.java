package sk.tuke.gamestudio.services.rating;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import sk.tuke.gamestudio.entity.Rating;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RatingServiceJDBCTest {
    private Rating rating;

    @Test
    public void addRatingTest() throws RatingException {
        RatingService service = new RatingServiceJDBC();
        rating = new Rating("MaxScorePlayer", "test_game", 5, new Date());

        service.setRating(rating);
        int ratingValue = service.getRating(rating.getGame(), rating.getPlayer());
        int avgRating = service.getAverageRating(rating.getGame());
        assertEquals(rating.getRating(), ratingValue);
        assertEquals(rating.getRating(), avgRating);

    }

    @AfterEach
    public void removeComment() throws RatingException {
        RatingServiceJDBC service = new RatingServiceJDBC();
        service.removeRating(rating);
    }

}
