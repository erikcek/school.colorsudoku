package sk.tuke.gamestudio.services.score;

import sk.tuke.gamestudio.entity.Score;

import java.util.List;

public interface ScoreService  {

    void addScore(Score s) throws ScoreException;

    List<Score> getTopScores(String game) throws ScoreException;
}
