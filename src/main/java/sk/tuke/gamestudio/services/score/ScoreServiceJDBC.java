package sk.tuke.gamestudio.services.score;

import sk.tuke.gamestudio.entity.Score;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ScoreServiceJDBC implements ScoreService {
    private static final String URL = "jdbc:postgresql://localhost:5432/gamestudio_old";

    private static final String LOGIN = "postgres";

    private static final String PASSWORD = "postgres";

    //language=postrgesql
    private static final String INSERT_COMMAND = "INSERT INTO score (player, points, game, playedon) VALUES (?, ?, ?, ?)";
    //language=postrgesql
    private static final String SELECT_COMMAND = "SELECT game, player, points, playedon FROM score WHERE game = ? ORDER BY points DESC";
    //language=postgresql
    private static final String REMOVE_COMMAND = "DELETE FROM score WHERE (player=? AND game=? AND playedon=? AND points=?)";

    @Override
    public void addScore(Score score) throws ScoreException {
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
             PreparedStatement stmt = connection.prepareStatement(INSERT_COMMAND)
        ) {
            stmt.setString(1, score.getPlayer());
            stmt.setInt(2, score.getPoints());
            stmt.setString(3, score.getGame());
            stmt.setTimestamp(4, new Timestamp(score.getPlayedOn().getTime()));
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new ScoreException("ADD_ERROR", e);
        }
    }

    @Override
    public List<Score> getTopScores(String gameName) throws ScoreException {
        List<Score> results = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
             PreparedStatement stmt = connection.prepareStatement(SELECT_COMMAND)
        ) {
            stmt.setString(1, gameName);

            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Score score = new Score(
                            rs.getString(1), rs.getString(2),
                            rs.getInt(3), rs.getTimestamp(4));
                    results.add(score);
                }
            }
        } catch (SQLException e) {
            throw new ScoreException("TOP_SCORE_ERROR", e);
        }
        return results;
    }

    public void removeScore(Score score) throws ScoreException {
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
             PreparedStatement stmt = connection.prepareStatement(REMOVE_COMMAND)
        ) {
            stmt.setString(1, score.getPlayer());
            stmt.setString(2, score.getGame());
            stmt.setTimestamp(3, new Timestamp(score.getPlayedOn().getTime()));
            stmt.setInt(4, score.getPoints());

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new ScoreException("REMOVE_ERROR", e);
        }
    }
}
