package sk.tuke.gamestudio.services.comment;

import org.springframework.web.client.RestTemplate;
import sk.tuke.gamestudio.entity.Comment;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class CommentServiceRestClient implements CommentService {
    private static final String URL = "http://localhost:8080/api/comment";

    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public void addComment(Comment comment) throws CommentException {
        try {
            restTemplate.postForEntity(URL, comment, Comment.class);
        }
        catch (Exception e) {
            throw new CommentException("ADD_ERROR", e);
        }
    }

    @Override
    public List<Comment> getComments(String game) throws CommentException {
        try {
            return Arrays.asList(Objects.requireNonNull(
                    restTemplate.getForEntity(URL + "/" + game, Comment[].class).getBody()));
        }
        catch (Exception e) {
            throw new CommentException("ADD_ERROR", e);
        }
    }

}
