package sk.tuke.gamestudio.services.comment;

import sk.tuke.gamestudio.entity.Comment;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CommentServiceJDBC implements CommentService {
    private static final String URL = "jdbc:postgresql://localhost:5432/gamestudio_old";

    private static final String LOGIN = "postgres";

    private static final String PASSWORD = "postgres";

    //language=postgresql
    private static String INSERT_COMMAND = "INSERT INTO comment (player, game, comment, commentedon) VALUES (?,?,?,?)";

    //language=postgresql
    private static String SELECT_COMMAND = "SELECT player, game, comment, commentedon FROM comment where game = ?";

    //language=postgresql
    private static final String REMOVE_COMMAND = "DELETE FROM comment WHERE (player=? AND game=? AND commentedon=? AND comment=?)";

    @Override
    public void addComment(Comment comment) throws CommentException {
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
             PreparedStatement stmt = connection.prepareStatement(INSERT_COMMAND);
        ) {
            stmt.setString(1, comment.getPlayer());
            stmt.setString(2, comment.getGame());
            stmt.setString(3, comment.getComment());
            stmt.setTimestamp(4, new Timestamp(comment.getCommentedOn().getTime()));
            stmt.executeUpdate();
        }
        catch (SQLException e) {
            throw new CommentException("ADD_ERROR", e);
        }
    }

    @Override
    public List<Comment> getComments(String game) throws CommentException {
        List<Comment> results = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
             PreparedStatement stmt = connection.prepareStatement(SELECT_COMMAND);
        ) {
            stmt.setString(1, game);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Comment comment = new Comment(rs.getString(1),
                            rs.getString(2),
                            rs.getString(3),
                            rs.getTimestamp(4));

                    results.add(comment);
                }
            }
        }
        catch (SQLException e) {
            throw new CommentException("GET_ERROR", e);
        }
        return results;
    }

    public void removeComment(Comment comment) throws CommentException {
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
             PreparedStatement stmt = connection.prepareStatement(REMOVE_COMMAND)
        ) {
            stmt.setString(1, comment.getPlayer());
            stmt.setString(2, comment.getGame());
            stmt.setTimestamp(3, new Timestamp(comment.getCommentedOn().getTime()));
            stmt.setString(4, comment.getComment());

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new CommentException("REMOVE_ERROR", e);
        }
    }
}
