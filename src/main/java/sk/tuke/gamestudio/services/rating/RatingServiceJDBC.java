package sk.tuke.gamestudio.services.rating;

import sk.tuke.gamestudio.entity.Rating;

import java.sql.*;

public class RatingServiceJDBC implements RatingService {
    private static final String URL = "jdbc:postgresql://localhost:5432/gamestudio_old";

    private static final String LOGIN = "postgres";

    private static final String PASSWORD = "postgres";

    //language=postgresql
    private static final String INSERT_COMMAND = "INSERT INTO rating (player, rating, game, ratedon) VALUES (?, ?, ?, ?)";

    //language=postgresql
    private static final String GET_AVERAGE_COMMAND = "SELECT AVG(rating) from rating WHERE game = ?";

    //language=postgresql
    private static final String GET_RATING_COMMAND = "SELECT rating from rating WHERE game = ? AND player = ?";

    //language=postgresql
    private static final String REMOVE_COMMAND = "DELETE FROM rating WHERE (player=? AND game=? AND ratedon=? AND rating=?)";


    @Override
    public void setRating(Rating rating) throws RatingException {
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
             PreparedStatement stmt = connection.prepareStatement(INSERT_COMMAND)
        ) {
            stmt.setString(1, rating.getPlayer());
            stmt.setInt(2, rating.getRating());
            stmt.setString(3, rating.getGame());
            stmt.setTimestamp(4, new Timestamp(rating.getRatedOn().getTime()));
            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RatingException("ADD_ERROR", e);
        }
    }

    @Override
    public int getAverageRating(String game) throws RatingException {
        int result = 0;
        try(Connection connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
            PreparedStatement stmt = connection.prepareStatement(GET_AVERAGE_COMMAND);
        ) {
            stmt.setString(1, game);

            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    result = rs.getInt(1);
                    System.out.println(result);
                }
                else {
                    throw new RatingException("NOT_FOUND - No rating for specified game");
                }
            }
        }
        catch (SQLException e) {
            throw new RatingException("AVG_ERROR", e);
        }
        return result;
    }

    @Override
    public int getRating(String game, String player) throws RatingException {
        int result = 0;
        try(Connection connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
            PreparedStatement stmt = connection.prepareStatement(GET_RATING_COMMAND);
        ) {
            stmt.setString(1, game);
            stmt.setString(2, player);

            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    result = rs.getInt(1);
                }
                else {
                    throw new RatingException("NOT_FOUND - No rating for specified game and player");
                }
            }
        }
        catch (SQLException e) {
            throw new RatingException("GET_ERROR", e);
        }

        return result;
    }

    public void removeRating(Rating rating) throws RatingException {
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, PASSWORD);
             PreparedStatement stmt = connection.prepareStatement(REMOVE_COMMAND)
        ) {
            stmt.setString(1, rating.getPlayer());
            stmt.setString(2, rating.getGame());
            stmt.setTimestamp(3, new Timestamp(rating.getRatedOn().getTime()));
            stmt.setInt(4, rating.getRating());

            stmt.executeUpdate();
        } catch (SQLException e) {
            throw new RatingException("REMOVE_ERROR", e);
        }
    }
}
