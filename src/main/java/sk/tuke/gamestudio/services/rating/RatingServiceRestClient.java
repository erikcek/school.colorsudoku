package sk.tuke.gamestudio.services.rating;

import org.springframework.web.client.RestTemplate;
import sk.tuke.gamestudio.entity.Rating;

import java.util.Objects;

public class RatingServiceRestClient implements RatingService {

    private static final String URL = "http://localhost:8080/api/rating";

    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public void setRating(Rating rating) throws RatingException {
        try {
            restTemplate.postForEntity(URL, rating, Rating.class);
        }
        catch (Exception e) {
            throw new RatingException("UNIQUE_USER_ERROR", e);
        }
    }

    @Override
    public int getAverageRating(String game) throws RatingException {
        try {
            return Objects.requireNonNull(restTemplate.getForEntity(URL + "/" + game + "/average", Integer.class)
                    .getBody());
        }
        catch (Exception e) {
            throw new RatingException("NOT_FOUND", e);
        }
    }

    @Override
    public int getRating(String game, String player) throws RatingException {

        try {
            return Objects.requireNonNull(restTemplate.getForEntity(URL + "/" + game + "/player/" + player, Integer.class).getBody());
        }
        catch (Exception e) {
            throw new RatingException("NOT_FOUND", e);
        }
    }
}
