package sk.tuke.gamestudio.services.rating;

import org.springframework.dao.DataIntegrityViolationException;
import sk.tuke.gamestudio.entity.Rating;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Transactional
public class RatingServiceJPA implements RatingService{
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public void setRating(Rating rating) throws RatingException {
        try {
            getRating(rating.getGame(), rating.getPlayer());
            entityManager.merge(rating);
        }
        catch (RatingException e) {
            entityManager.persist(rating);
        }
        catch (Exception e) {
            throw new RatingException(e.getMessage());
        }
    }

    @Override
    public int getAverageRating(String game) throws RatingException {
        try {
        return (entityManager.createNamedQuery("Rating.getAverageRating", Double.class)
                .setParameter("game", game)
                .getSingleResult()).intValue();
        } catch (Exception e) {
            throw new RatingException(e.getMessage());
        }
    }

    @Override
    public int getRating(String game, String player) throws RatingException {
        try {
            return entityManager.createNamedQuery("Rating.getRating", Integer.class)
                    .setParameter("game", game)
                    .setParameter("player", player).getSingleResult();
        } catch (Exception e) {
            throw new RatingException(e.getMessage());
        }
    }
}
