package sk.tuke.gamestudio.game.colorSudoku.core;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SudokuGenerator {


    private String[] sudokus9x9 = {
        "004300209005009001070060043006002087190007400050083000600000105003508690042910300",
        "040100050107003960520008000000000017000906800803050620090060543600080700250097100",
        "600120384008459072000006005000264030070080006940003000310000050089700000502000190",
        "497200000100400005000016098620300040300900000001072600002005870000600004530097061"
    };


    public int[][] get9x9Sudoku() {
        String sudoku;
        int[][] result = new int[9][9];
        Random random = new Random();

        try {
            URL fileUrl = getClass().getClassLoader().getResource("sudoku.csv");
            File sudokuFile = new File(fileUrl.getFile());
            Stream<String> lines = Files.lines(sudokuFile.toPath());
            int linesToSkip = random.nextInt(65530);
            sudoku = lines.skip(linesToSkip).findFirst().get().split(",")[0];
            String finalSudoku = sudoku;
            IntStream.range(0,9).forEach(i -> IntStream.range(0,9).forEach(j -> {
                result[i][j] = Integer.parseInt(finalSudoku.substring(i*9 + j, i*9 + j + 1));
            }));
            return result;
        } catch (IOException | NullPointerException e) {
            sudoku = this.sudokus9x9[random.nextInt(this.sudokus9x9.length)];
            String finalSudoku1 = sudoku;
            IntStream.range(0,9).forEach(i -> IntStream.range(0,9).forEach(j -> {
                result[i][j] = Integer.parseInt(finalSudoku1.substring(i*9 + j, i*9 + j + 1));
            }));
            return result;
        }
    }
}
