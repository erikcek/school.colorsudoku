package sk.tuke.gamestudio.game.colorSudoku.core;

public enum GameState {
    PLAYING, WON
}
