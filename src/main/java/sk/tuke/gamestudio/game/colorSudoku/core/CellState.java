package sk.tuke.gamestudio.game.colorSudoku.core;

public enum CellState {
    EMPTY, FILLED
}
