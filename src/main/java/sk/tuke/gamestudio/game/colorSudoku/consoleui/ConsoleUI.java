package sk.tuke.gamestudio.game.colorSudoku.consoleui;

import org.springframework.beans.factory.annotation.Autowired;
import sk.tuke.gamestudio.entity.Comment;
import sk.tuke.gamestudio.entity.Rating;
import sk.tuke.gamestudio.entity.Score;
import sk.tuke.gamestudio.game.colorSudoku.core.*;
import sk.tuke.gamestudio.services.rating.RatingService;
import sk.tuke.gamestudio.services.comment.CommentException;
import sk.tuke.gamestudio.services.comment.CommentService;
import sk.tuke.gamestudio.services.rating.RatingException;
import sk.tuke.gamestudio.services.score.ScoreException;
import sk.tuke.gamestudio.services.score.ScoreService;

import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ConsoleUI {
    private SudokuGame colorSudoku;
    private int fieldSize;
    private int subfieldSize;
    private static final Pattern COLOR_INPUT_PATTERN = Pattern.compile(
            "([0-8]) ?([0-8]) ?(RED|GREEN|BLUE|YELLOW|CYAN|PURPLE|GREY|BRIGHT_PURPLE|WHITE|DELETE)");
    private static  final Pattern RATING_INPUT_PATTERN = Pattern.compile("[1-5]");
    private static final String GAME_NAME = "color-sudoku";
    private String sectionIdentifier = "M";

    @Autowired
    private CommentService commentService;
    @Autowired
    private RatingService ratingService;
    @Autowired
    private ScoreService scoreService;

    private void initializeGame() {
        this.colorSudoku = new SudokuGame();
        this.fieldSize = colorSudoku.getFieldSize();
        this.subfieldSize = colorSudoku.getSubfieldSize();
    }

    private void printSudokuField() {
        printTopLine();

        Field<Color> riddle = colorSudoku.getSudokuMatrix();
        for (int i = 0; i < fieldSize; i++) {
            if (i > 0 && i % subfieldSize == 0) {
                printMiddleLine();
            }
            for (int j = 0; j < fieldSize; j++) {
                if (j % subfieldSize == 0) {
                    if (j == 0)
                        System.out.print(i + " ");
                    System.out.print("\u2502");
                }
                printSudokuCell(riddle.getCell(i,j));
            }
            System.out.println("\u2502");
        }

        printBottomLine();
    }

    private void printTopLine() {
        System.out.print("  ");
        for (int i = 0; i< fieldSize; i++) {
            if ( i != 0 && i % subfieldSize == 0)
                System.out.print("  ");
            System.out.print(" " + i + " ");
        }
        System.out.println();
        System.out.print("  \u250C");
        for (int i = 0; i< fieldSize; i++) {
            if ( i != 0 && i % subfieldSize == 0) {
                System.out.print("\u252C");
            }

            System.out.print("\u2500\u2500\u2500");

        }
        System.out.println("\u2510");
    }

    private void printMiddleLine() {
        System.out.print("  \u251C");
        for (int i = 0; i< fieldSize; i++) {
            if ( i != 0 && i % subfieldSize == 0) {
                System.out.print("\u253C");
            }

            System.out.print("\u2500\u2500\u2500");

        }
        System.out.println("\u2524");
    }

    private void printBottomLine() {
        System.out.print("  \u2514");
        for (int i = 0; i< fieldSize; i++) {
            if ( i != 0 && i % subfieldSize == 0) {
                System.out.print("\u2534");
            }

            System.out.print("\u2500\u2500\u2500");

        }
        System.out.println("\u2518");
    }

    private void printSudokuCell(Cell<Color> cell) {
        if (cell.getValue() != null) {
            if (cell instanceof Clue) {
                System.out.print(cell.getValue().getValue() + " \u25A2 " + Color.reset());
            }
            else {
                System.out.print(cell.getValue().getValue() + " \u25FC " + Color.reset());
            }
        }
        else {
            System.out.print("   ");
        }
    }

    private void handleColorInput() {
        while (true) {
            System.out.println("Enter value for cell in format ROW COLUMN COLOR (e.g. 0, 0, RED): ");
            System.out.print("Enter value for cell in format ROW COLUMN DELETE (e.g. 0, 0, DELTE): \n");
            System.out.println("Press X to exit game, press M to get back to menu.");
            Arrays.stream(Color.values()).forEach(c -> System.out.print(c.getValue() + c + " " + Color.reset()));
            System.out.println();

            String input = new Scanner(System.in).nextLine().trim().toUpperCase();

            if ("X".equals(input))
                System.exit(0);

            if ("M".equals(input)) {
                sectionIdentifier = "M";
                return;
            }
            Matcher matcher = COLOR_INPUT_PATTERN.matcher(input);
            if (matcher.matches()) {
                try {
                    int x = Integer.parseInt(matcher.group(1));
                    int y = Integer.parseInt(matcher.group(2));
                    if (matcher.group(3).equals("DELETE")) {
                        handleRemoveGuess(x, y);
                    }
                    else {
                        Color color = Color.valueOf(matcher.group(3));
                        colorSudoku.setGuess(x, y, color);
                    }
                    return;
                } catch (InvalidParameterException e) {
                    printColorMessage("Selected cell is clue, therefor cannot be changed.", Color.RED);
                }
            }
        }

    }

    private void handleRemoveGuess(int x, int y) {
        try {
            colorSudoku.removeGuess(x,y);
        }
        catch (InvalidParameterException e) {
            printColorMessage("Selected cell is clue, therefore cannot be deleted", Color.RED);
        }
    }

    private void playGame() {
        initializeGame();
        while (colorSudoku.getState() != GameState.WON) {
            System.out.println("Your score: " + colorSudoku.getScore());
            printSudokuField();
            handleColorInput();
            if (sectionIdentifier.equals("M")) {
                break;
            }
        }
        if (colorSudoku.getState() == GameState.WON) {
            System.out.println("Congratulation, you won!");
            System.out.println("Your final score is " + colorSudoku.getScore());
            sectionIdentifier="M";
            try {
                scoreService.addScore(new Score(GAME_NAME, System.getProperty("user.name"), colorSudoku.getScore(), new Date()));
            } catch (ScoreException e) {
                printColorMessage("There was a problem adding your score in scoreboard.", Color.RED);
            }
        }
    }

    public void play() {
        while(!sectionIdentifier.equals("X")) {
            switch (sectionIdentifier) {
                case "N":
                    playGame();
                    break;
                case "C":
                    printCommentSectionMenu();
                    handleCommentSectionMenuInput();
                    break;
                case "R":
                    printRatingSectionMenu();
                    handleRantingSectionInput();
                    break;
                case "S":
                    handleGetTopScore();
                    break;
                default:
                    printMainMenu();
                    handleMainMenuInput();
            }
        }
    }

    private void printMainMenu() {
        printColorMessage("Main menu:", Color.BLUE);
        System.out.println("N - play new game");
        System.out.println("C - go to comment section");
        System.out.println("R - go to rating section");
        System.out.println("S - get top scores");
        System.out.println("x - exit game");
    }

    private void printCommentSectionMenu() {
        printColorMessage("Comment section:", Color.BLUE);
        System.out.println("A - add comment to game");
        System.out.println("S - show all comments");
        System.out.println("B - get back to main menu");
        System.out.println("X - exit game");
    }

    private void printRatingSectionMenu() {
        printColorMessage("Rating section:", Color.BLUE);
        System.out.println("A - add rating to game");
        System.out.println("G - get average rating");
        System.out.println("U - get rating from specific user");
        System.out.println("B - get back to main menu");
        System.out.println("X - exit game");
    }

    private void handleMainMenuInput() {
        String input = new Scanner(System.in).nextLine().trim().toUpperCase();
        switch (input) {
            case "X":
                System.exit(0);
                break;
            case "N":
            case "C":
            case "S":
            case "R":
                sectionIdentifier = input;
                break;
            default:
                sectionIdentifier = "UNKNOWN_MAIN_MENU_OPTION";
                break;
        }
    }

    private void handleCommentSectionMenuInput() {
        String input = new Scanner(System.in).nextLine().trim().toUpperCase();
        switch (input) {
            case "X":
                System.exit(0);
                break;
            case "A":
                handleAddComment();
                break;
            case "S":
                printAllComments();
                break;
            case "B":
                sectionIdentifier = "M";
                break;
            default:
                sectionIdentifier = "UNKNOWN_COMMENT_MENU_OPTION";
                break;
        }
    }

    private void handleAddComment() {
        System.out.println("Print your comment.");
        String input = new Scanner(System.in).nextLine().trim();
        Comment newComment = new Comment(System.getProperty("user.name"), GAME_NAME, input, new Date());
        try {
            commentService.addComment(newComment);
            printColorMessage("Your comment has been added.", Color.GREEN);
        } catch (CommentException e) {
            printColorMessage("There was a problem with adding new comment.", Color.RED);
        }
    }

    private void printAllComments() {
        try {
            List<Comment> comments = commentService.getComments(GAME_NAME);
            comments.forEach(c -> printColorMessage(c.toString(), Color.CYAN));
        } catch (CommentException e) {
            printColorMessage("There was a problem with loading comments.", Color.RED);
        }

    }

    private void printColorMessage(String message, Color c) {
        System.out.println(c.getValue() + message + Color.reset());
    }

    private void handleRantingSectionInput() {
        String input = new Scanner(System.in).nextLine().trim().toUpperCase();
        switch (input) {
            case "X":
                System.exit(0);
                break;
            case "A":
                handleAddRating();
                break;
            case "G":
                handleAverageRating();
                break;
            case "U":
                handleSearchRating();
                break;
            case "B":
                sectionIdentifier = "M";
                break;
            default:
                sectionIdentifier = "UNKNOWN_COMMENT_MENU_OPTION";
                break;
        }
    }

    private void handleAddRating() {
        Matcher matcher;
        String input;
        do {
            System.out.println("Print your rating. (form 1 to 5)");

            input = new Scanner(System.in).nextLine().trim();
            if ("B".equals(input)) {
                sectionIdentifier = "M";
                return;
            }
            matcher = RATING_INPUT_PATTERN.matcher(input);
        }
        while(!matcher.matches());

        Rating newRating = new Rating(System.getProperty("user.name"), GAME_NAME, Integer.parseInt(input), new Date());
        try {
            ratingService.setRating(newRating);
            printColorMessage("Your rating has been added.", Color.GREEN);
        } catch (RatingException e) {
            if (e.getMessage().contains("duplicate key value violates unique constraint")
                    || e.getMessage().contains("UNIQUE_USER_ERROR"))
                printColorMessage("It looks like you have already rated this game.", Color.RED);
            else
                printColorMessage("There was a problem with adding new rating.", Color.RED);
        }
    }

    private void handleAverageRating() {
        try {
            int avgRating = ratingService.getAverageRating(GAME_NAME);
            printColorMessage("Average rating of this game is: " + avgRating, Color.GREEN);
        } catch (RatingException e) {
            if (e.getMessage().contains("NOT_FOUND"))
                printColorMessage("There is no rating for this game yet", Color.RED);
            else
                printColorMessage("There eas a problem with loading average rating.", Color.RED);
        }
    }

    private void handleSearchRating() {
        System.out.println("Type name of user.");
        String input = new Scanner(System.in).nextLine().trim();
        try {
            int userRating = ratingService.getRating(GAME_NAME, input);
            printColorMessage("Rating of user " + input + " is: " + userRating, Color.GREEN);
        }
        catch (RatingException e) {
            if (e.getMessage().contains("NOT_FOUND"))
                printColorMessage("User" + input + "have not rated the game yet.", Color.RED);
            else
                printColorMessage("There eas a problem with loading user rating.", Color.RED);
        }
    }

    private void handleGetTopScore() {
        try {
            List<Score> scores = scoreService.getTopScores(GAME_NAME);
            if (scores.size() == 0) {
                printColorMessage("There is no score in scoreboard yet.", Color.YELLOW);
            } else {
                scores.stream().limit(10).forEach(s -> printColorMessage(s.toString(), Color.CYAN));
            }
        } catch (ScoreException e) {
            printColorMessage("There eas a problem with loading top scores.", Color.RED);
        }
        sectionIdentifier = "M";
    }

}
