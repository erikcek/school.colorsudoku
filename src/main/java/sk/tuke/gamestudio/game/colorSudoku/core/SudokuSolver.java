package sk.tuke.gamestudio.game.colorSudoku.core;

import java.util.*;
import java.util.stream.IntStream;

public class SudokuSolver {

    private int fieldSize;
    private int subfieldSize;
    private int[][] riddle;
    private int[][] solution;
    private Stack<OptionQueue> gameOptions;

    public SudokuSolver(int fieldSize, int subfieldSize, int[][] riddle) {
        this.fieldSize = fieldSize;
        this.subfieldSize = subfieldSize;
        this.riddle = riddle;
        this.gameOptions = new Stack<>();
        this.solution = new int[fieldSize][fieldSize];
        IntStream.range(0, fieldSize).forEach(i -> IntStream.range(0, fieldSize).forEach(j -> solution[i][j] = riddle[i][j]));

    }


   public int[][] solveRiddle() {
        int x,y;
        for (x = 0; x< fieldSize; x++) {
            for (y = 0; y< fieldSize; y++) {
                if (riddle[x][y] == 0) {
                    Set<Integer> uniqueElements = getUniqueItems(x, y);
                    if (uniqueElements.size() > 0) {
                        insertValue(x, y, uniqueElements);
                    }
                    else {
                        OptionQueue options = gameOptions.pop();
                        x = options.getX();
                        y = options.getY();
                        solution[x][y] = options.dequeue();
                        if (options.size() > 0) {
                            gameOptions.push(options);
                        }
                        getClearSolution(x, y);
                    }
                }
            }
        }
        return solution;
   }

    private void getClearSolution(int x, int y) {
        IntStream.range(0, fieldSize).forEach(i -> IntStream.range(0, fieldSize).forEach(j -> {
            if ( !((i<x) || (x==i && j<=y)) ) {
                solution[i][j] = riddle[i][j];
            }
        }));
    }

    private void insertValue(int x, int y, Set<Integer> options) {

        if (x < 0 || x > fieldSize || y < 0 || y > fieldSize) {
            throw new ArrayIndexOutOfBoundsException("no such element in the field");
        }

       OptionQueue cellOptions = new OptionQueue(x, y);
       cellOptions.enqueueFromSet(options);

       solution[x][y] = cellOptions.dequeue();

       if (cellOptions.size() > 0) {
           gameOptions.push(cellOptions);
       }

   }

    private Set<Integer> getUniqueItems(int x, int y) {
        Integer[] row = Arrays.stream(this.getRow(x)).filter(i -> i != 0).boxed().toArray(Integer[]::new);
        Integer[] column = Arrays.stream(this.getColumn(y)).filter(i -> i != 0).boxed().toArray(Integer[]::new);
        Integer[] subfield = Arrays.stream(this.getSubfieldOf(x, y)).flatMapToInt(Arrays::stream)
               .filter(i-> i != 0).boxed().toArray(Integer[]::new);
        Set<Integer> uniqueElem = new HashSet<>(Arrays.asList(row));
        uniqueElem.addAll(Arrays.asList(column));
        uniqueElem.addAll(Arrays.asList(subfield));
        Set<Integer> result = new HashSet<>();
        IntStream.range(1, fieldSize + 1).forEach(i -> {
            if (!uniqueElem.contains(i)) {
                result.add(i);
            }
        });
        return result;
    }

    private int[] getRow(int x) {
        int[] result = new int[fieldSize];
        if (x < 0 || x > fieldSize) {
            throw new ArrayIndexOutOfBoundsException("no such row in the field");
        }
        IntStream.range(0, fieldSize).forEach(i -> result[i] = solution[x][i]);
        return result;
    }

    private int[] getColumn(int y) {
        int[] result = new int[fieldSize];
        if (y < 0 || y > fieldSize) {
            throw new ArrayIndexOutOfBoundsException("no such row in the field");
        }
        IntStream.range(0, fieldSize).forEach(i -> result[i] = solution[i][y]);
        return result;
    }

    private int[][] getSubfieldOf(int x, int y) {
        int[][] result = new int[subfieldSize][subfieldSize];
        int[] xIndexes = findRangeOf(x).toArray();
        int[] yIndexes = findRangeOf(y).toArray();

        IntStream.range(0, subfieldSize).forEach(i -> IntStream.range(0, subfieldSize).forEach(j ->
                result[i][j] = solution[xIndexes[i]][yIndexes[j]]));
        return result;
    }

    private IntStream findRangeOf(int index) {
        int numberOfSubfields = fieldSize / subfieldSize;

        for (int i=0; i<numberOfSubfields; i++) {
            int lowerBound = i* subfieldSize;
            int upperBound = (i+1) * subfieldSize;
            if (index >= lowerBound && index < upperBound) {
                return IntStream.range(lowerBound, upperBound);
            }
        }
        throw new ArrayIndexOutOfBoundsException("wrong index of element");
    }

}
