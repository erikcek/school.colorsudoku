package sk.tuke.gamestudio.game.colorSudoku;

import sk.tuke.gamestudio.game.colorSudoku.consoleui.ConsoleUI;
import sk.tuke.gamestudio.game.colorSudoku.core.Color;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        ConsoleUI cui = new ConsoleUI();
        cui.play();

    }

    private static Color getRangomColor() {
        int randomIndex = new Random().nextInt(Color.values().length);
        return Color.values()[randomIndex];
    }
}
