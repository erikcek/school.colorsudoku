package sk.tuke.gamestudio.game.colorSudoku.core;
import java.security.InvalidParameterException;
import java.util.stream.IntStream;

public class SudokuGame {

    private int[][] sudokuRiddle;
    private int[][] sudokuSolution;
    private Field<Color> sudokuMatrix;
    private GameState state;
    private int fieldSize;
    private int subfieldSize;
    private long starTime;

    public SudokuGame() {
        fieldSize = 9;
        subfieldSize = 3;
        SudokuGenerator generator = new SudokuGenerator();
        sudokuRiddle = generator.get9x9Sudoku();
        SudokuSolver solver = new SudokuSolver(fieldSize, subfieldSize, sudokuRiddle);
        sudokuSolution = solver.solveRiddle();
        sudokuMatrix = new Field<>(fieldSize,subfieldSize);
        fillSudokuMatrix();
        state = GameState.PLAYING;
        starTime = System.currentTimeMillis();
    }

    private void fillSudokuMatrix() {
        IntStream.range(0,fieldSize).forEach(i -> IntStream.range(0,fieldSize).forEach(j -> {
            if (sudokuRiddle[i][j] != 0) {
                sudokuMatrix.setCell(i, j, new Clue<>(getColorFrom(sudokuRiddle[i][j] - 1)));
            }
            else {
                sudokuMatrix.setCell(i, j, new Guess<>(getColorFrom(sudokuSolution[i][j] - 1)));
            }
        }));
    }

    public void setGuess(int x, int y, Color guessColor) throws InvalidParameterException {
        Cell<Color> sudokuCell = sudokuMatrix.getCell(x,y);
        if(sudokuCell instanceof Guess) {
            ((Guess<Color>) sudokuCell).setValue(guessColor);
            if (isGameWon()) {
                state = GameState.WON;
            }
        }
        else {
            throw new InvalidParameterException("Non settable cell at (x,y) coordinates");
        }
    }

    public void removeGuess(int x, int y) throws InvalidParameterException {
        Cell sudokuCell = sudokuMatrix.getCell(x,y);
        if(sudokuCell instanceof Guess) {
            ((Guess) sudokuCell).removeValue();
            if (state == GameState.WON) {
                state = GameState.PLAYING;
            }
        }
        else {
            throw new InvalidParameterException("Non settable cell at (x,y) coordinates");
        }
    }

    private Color getColorFrom(int number) {
        if (number > Color.values().length || number < 0) {
            throw new IndexOutOfBoundsException("No corresponding color for number");
        }
        return Color.values()[number];
    }

    private boolean isGameWon() {
        for (int i = 0; i < fieldSize; i++)
            for (int j = 0; j < fieldSize; j++) {
                Cell sudokuCell = sudokuMatrix.getCell(i, j);
                if (sudokuCell.getState() == CellState.EMPTY) {
                    return false;
                }
                if (sudokuCell instanceof Guess) {
                    if (!((Guess) sudokuCell).checkValue()) {
                        return false;
                    }
                }
            }
        return true;
    }

    public GameState getState() {
        return state;
    }

    public int getFieldSize() {
        return fieldSize;
    }

    public int getSubfieldSize() {
        return subfieldSize;
    }

    public Field<Color> getSudokuMatrix() {
        return sudokuMatrix;
    }

    private int getPlayingTime() {
        return ((int) (System.currentTimeMillis() - starTime)) / 1000;
    }

    private int getNumberOfEmptyRiddleCells() {
        int result = 0;
        for (int i = 0; i < fieldSize; i++)
            for (int j = 0; j < fieldSize; j++) {
                if (sudokuRiddle[i][j] == 0) {
                    result++;
                }
            }
        return result;
    }

    public int getScore() {
        return Math.max((getNumberOfEmptyRiddleCells() * 50 - getPlayingTime()), 0);
    }
}
