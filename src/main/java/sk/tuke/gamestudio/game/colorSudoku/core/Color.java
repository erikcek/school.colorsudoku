package sk.tuke.gamestudio.game.colorSudoku.core;

public enum Color {
     RED("\u001B[91m"),
     GREEN("\u001B[92m"),
     BLUE("\u001B[94m"),
     YELLOW("\u001B[93m"),
     CYAN("\u001B[36m"),
     PURPLE("\u001B[35m"),
     GREY("\u001B[38m"),
     BRIGHT_PURPLE("\u001B[95m"),
     WHITE("\u001B[30m");

     private String value;

     Color(String value) {
          this.value = value;
     }

     public String getValue() {
          return value;
     }

     public static String reset() {
          return "\u001B[0m";
     }
}
