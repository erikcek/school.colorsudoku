package sk.tuke.gamestudio.game.colorSudoku.core;

public class Clue<T> extends Cell<T> {
    public Clue(T value) {
        this.value = value;
        this.state = CellState.FILLED;
    }
}
