package sk.tuke.gamestudio.game.colorSudoku.core;

public abstract class Cell<T> {
    protected T value;
    protected CellState state = CellState.EMPTY;

    public T getValue() {
        if (state == CellState.FILLED) {
            return value;
        }
        return null;
    }

    public CellState getState() {
        return state;
    }
}


/*
[<<Abstract Class>>Cell{bg:wheat}]^[<<Class>>Clue{bg:wheat}]
[<<Abstract Class>>Cell]^[<<Class>>Guess{bg:wheat}]

[<<Class>>Field{bg:wheat}]++->[<<Abstract Class>>Cell]
[<<Abstract Class>>Cell]->[<<Enum>>CellState{bg:palegreen}]

[<<Class>>SudokuSolver]->[<<Class>>OptionQueue]

[SudokuGame]->[<<Class>>SudokuGenerator]
[SudokuGame]->[<<Class>>SudokuSolver]
[SudokuGame]->[<<Class>>Field]
[SudokuGame]->[<<Enum>>GameState]
[SudokuGame]->[<<Enum>>Color]

[ConsoleUI]<>->[SudokuGame]

[<<Abstract Class>>Cell|- value: T| + getValue(): T; + getState(): CellState]

[<<Enum>>CellState| EMPTY; FILLED]

[<<Class>>Guess| - realValue: T| + setValue(value: T): void; + removeValue(): void; + setState(cell: CellState): void; # checkValue(): boolean]

[<<Enum>>Color|RED;GREEN;BLUE;YELLOW;CYAN;PURPLE;GREY;BRIGHT_PURPLE;WHITE]

[<<Enum>>GameState| PLAYING;WON]

[<<Class>>Field|- fieldSize: int|+ getCell(): Cell; + setCell(x: int, y: int, cell: Cell): void]

[<<Class>>OptionQueue| - options: LinkedList; -x: int; - y: int| + enqueue(value: int): void; + dequeue(): int; + getX(): int; + getY(): int; + getSize(): int; + enqueueFromSet(values: Set): void]

[<<Class>>SudokuSolver| - fieldSize: int; - subfieldSize: int; - gameOptions: Stack﹤OptionQueue﹥| + solveRiddle(): int［］［］]

[<<Class>>SudokuGenerator| + get9x9Sudoku(): int［］［］]

[<<Class>>SudokuGame| + setGuess(x: int, y: int, color: Color): void; + removeGuess(x: int, y: int); + getState(): CellState; + getScore(): int; + getSudokuMatrix(): Field﹤Color﹥]

[<<Class>>ConsoleUI| + play(): void]







 */
