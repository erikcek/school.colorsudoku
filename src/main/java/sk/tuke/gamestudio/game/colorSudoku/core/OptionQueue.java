package sk.tuke.gamestudio.game.colorSudoku.core;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Set;

public class OptionQueue {
    private LinkedList<Integer> options;
    private int x;
    private int y;

    public OptionQueue(int x, int y) {
        this.x = x;
        this.y = y;
        this.options = new LinkedList<>();
    }

    public void enqueueFromSet(Set<Integer> values) {
        values.forEach(this::equeue);
    }

    public void equeue(int value) {
        this.options.push(value);
    }

    public int dequeue() {
        if (size() == 0) {
            throw new NoSuchElementException("Unable to dequeue because queue is empty.");
        }
        int value = this.options.getLast();
        this.options.removeLast();
        return value;
    }

    public int size() {
        return this.options.size();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
