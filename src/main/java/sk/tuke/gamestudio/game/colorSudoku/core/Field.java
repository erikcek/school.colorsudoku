package sk.tuke.gamestudio.game.colorSudoku.core;

public class Field<T> {
    private final int fieldSize;
    private final Cell<T>[][] cells;

    public Field(int fieldSize, int subfieldSize) {
        this.fieldSize = fieldSize;
        
        this.cells = (Cell<T>[][]) new Cell[fieldSize][fieldSize];
    }

    void setCell(int x, int y, Cell<T> cell) {
        if (x < 0 || x > fieldSize || y < 0 || y > fieldSize) {
            throw new ArrayIndexOutOfBoundsException("no such cell in the field");
        }
        if (cells[x][y] != null) {
            throw new IllegalStateException("cell already populated");
        }
        cells[x][y] = cell;
    }

    public Cell<T> getCell(int x, int y) {
        if (x < 0 || x > fieldSize || y < 0 || y > fieldSize) {
            throw new ArrayIndexOutOfBoundsException("no such cell in the field");
        }
        return cells[x][y];
    }

}
