package sk.tuke.gamestudio.game.colorSudoku.core;

public class Guess<T> extends Cell<T> {
    private T realValue;

    public Guess(T realValue) {
        this.realValue = realValue;
        this.state = CellState.EMPTY;
    }

    public void setValue(T value) {
        this.value = value;
        setState(CellState.FILLED);
    }

    public void removeValue() {
        setState(CellState.EMPTY);
    }

    private void setState(CellState state) {
        this.state = state;
    }

    protected boolean checkValue() {
        return realValue == value;
    }
}
