package sk.tuke.gamestudio.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;


@Entity
@NamedQueries({
        @NamedQuery(name = "Rating.getAverageRating", query = "SELECT AVG(r.rating) from Rating r WHERE r.game=:game"),
        @NamedQuery(name = "Rating.getRating", query = "SELECT r.rating FROM Rating r WHERE r.game=:game AND r.player=:player")
})
public class Rating implements Serializable {
    @GeneratedValue
    private int ident;
    @Id
    private String player;
    @Id
    private String game;
    private int rating;
    private Date ratedOn;

    public Rating() {
    }

    public Rating(String player, String game, int rating, Date createdDate) {
        this.player = player;
        this.game = game;
        this.rating = rating;
        this.ratedOn = createdDate;
    }

    public int getIdent() {
        return ident;
    }

    public void setIdent(int ident) {
        this.ident = ident;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Date getRatedOn() {
        return ratedOn;
    }

    public void setRatedOn(Date ratedOn) {
        this.ratedOn = ratedOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rating raiting = (Rating) o;
        return rating == raiting.rating &&
                Objects.equals(player, raiting.player) &&
                Objects.equals(game, raiting.game) &&
                Objects.equals(ratedOn, raiting.ratedOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(player, game, rating, ratedOn);
    }


    @Override
    public String toString() {
        return "Rating: \n" +
                "ident:" + ident + '\n' +
                "player: " + player + '\n' +
                "game: " + game + '\n' +
                "rating: " + rating + "\n" +
                "rated on: " + ratedOn.toString() + "\n";
    }
}

