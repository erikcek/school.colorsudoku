package sk.tuke.gamestudio.server.webservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sk.tuke.gamestudio.entity.Rating;
import sk.tuke.gamestudio.services.rating.RatingException;
import sk.tuke.gamestudio.services.rating.RatingService;


@RestController
@RequestMapping("/api/rating")
public class RatingRestService {
    @Autowired
    private RatingService ratingService;

    @GetMapping("/{game}/average")
    public int getAverageRating(@PathVariable String game) throws RatingException {

            return ratingService.getAverageRating(game);
    }

    @GetMapping("/{game}/player/{player}")
    public int getPlayerRating(@PathVariable String player, @PathVariable String game) throws RatingException {
        return ratingService.getRating(game, player);
    }

    @PostMapping
    public void addRating(@RequestBody Rating rating) throws RatingException {
        ratingService.setRating(rating);
    }
}
