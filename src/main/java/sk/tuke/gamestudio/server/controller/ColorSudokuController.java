package sk.tuke.gamestudio.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import sk.tuke.gamestudio.entity.Comment;
import sk.tuke.gamestudio.entity.Rating;
import sk.tuke.gamestudio.entity.Score;
import sk.tuke.gamestudio.game.colorSudoku.core.*;
import sk.tuke.gamestudio.services.comment.CommentException;
import sk.tuke.gamestudio.services.comment.CommentService;
import sk.tuke.gamestudio.services.rating.RatingException;
import sk.tuke.gamestudio.services.rating.RatingService;
import sk.tuke.gamestudio.services.score.ScoreService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;


@Controller
@Scope(WebApplicationContext.SCOPE_SESSION)
@RequestMapping("/colorSudoku")
public class ColorSudokuController {

    @Autowired
    private ScoreService scoreService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private RatingService ratingService;

    @Autowired
    private UserController userController;


    private final String gameName = "colorSudoku";

    private SudokuGame game;
    private int  fieldSize;
    private int  subfielSize;
    private String[][] gameField;

    private boolean setGuess = true;
    private boolean deleteGuess;
    private Color color = Color.RED;

    @RequestMapping
    public String colorSudoku(@RequestParam(value = "row", required = false) String row,
                              @RequestParam(value = "column", required = false) String column,
                              Model model) {
        try {
            if (game == null) {
                newGame();
                updateGameField(model);
            }
            if (setGuess && row != null && column != null) {
                game.setGuess(Integer.parseInt(row), Integer.parseInt(column), color);
                updateGameField(model);
                if (game.getState() == GameState.WON) {
                    model.addAttribute("won", true);
                    if (userController.isLogged()) {
                        scoreService.addScore(new Score("colorSudoku", userController.getLoggedUser(), game.getScore(), new Date()));
                    }
                }

            }
            else if (deleteGuess && row != null && column != null) {
                game.removeGuess(Integer.parseInt(row), Integer.parseInt(column));
                updateGameField(model);
            }
        } catch (Exception e) {
            addSudokuErrorMessage(model, "You can not change clue cells.");
        }
        updateGameField(model);
        return "colorSudoku";
    }

    @RequestMapping("/setGuess")
    public String sudokuSetGues(Model model) {
        if (game == null) {
            return "redirect:/colorSudoku";
        }
        setGuess = !setGuess;
        deleteGuess = !deleteGuess;
        updateGameField(model);
        return "colorSudoku";
    }

    @RequestMapping("/removeGuess")
    public String sudokuRemoveGues(Model model) {
        if (game == null) {
            return "redirect:/colorSudoku";
        }
        deleteGuess = !deleteGuess;
        setGuess = !setGuess;
        updateGameField(model);
        return "colorSudoku";
    }

    @RequestMapping("/setColor")
    public String sudokuSetColor(@RequestParam(value = "color", required = true) Color c,
                                 Model model) {
        if (game == null) {
            return "redirect:/colorSudoku";
        }
        color = c;
        updateGameField(model);
        return "colorSudoku";
    }

    @RequestMapping("/new")
    public String sudokuNewGame(Model model) {
        newGame();
        updateGameField(model);
        return "colorSudoku";
    }

    @RequestMapping(value = "/addComment", method = RequestMethod.POST)
    public String sudokuAddComment(String commentInput, Model model) {
        if (!userController.isLogged()) {
            addCommentErrorMessage(model, "You must log in first");
        }
        else if (commentInput == null || commentInput.equals("")) {
            addCommentErrorMessage(model, "You can not post empty comment");
        }
        else {
            try {
                commentService.addComment(new Comment(userController.getLoggedUser(), gameName, commentInput, new Date()));
                addCommentSuccessMessage(model, "Comment uploaded successfully");
            } catch (CommentException e) {
                addCommentErrorMessage(model, "There was a problem with posting your comment.");
            }
        }

        updateGameField(model);
        return "colorSudoku";
    }

    @RequestMapping("/addRating")
    public String rateGame(@RequestParam(value = "rating", required = true) String rating, Model model) {
        if (!userController.isLogged()) {
            addRatingErrorMessage(model, "You are not logged in");
        }
        else {
            try {
                ratingService.setRating(new Rating(userController.getLoggedUser(), gameName, Integer.parseInt(rating), new Date()));
                addRatingSuccessMessage(model, "Your rating has been added");
            } catch (Exception e) {
                addRatingErrorMessage(model, "There was a problem to add your rating :/");
            }
        }
        updateGameField(model);
        return "colorSudoku";
    }

    @RequestMapping("/howToPlay")
    public String howToPlay(Model model) {
        return "howToPlay";
    }

    private void newGame() {
        game = new SudokuGame();
        fieldSize = game.getFieldSize();
        subfielSize = game.getSubfieldSize();
        Field<Color> matrix = game.getSudokuMatrix();
        gameField = new String[fieldSize][fieldSize];
    }

    private void addSudokuErrorMessage(Model model, String msg) {
        model.addAttribute("sudokuErrorMessage", msg);
    }

    private void addCommentErrorMessage(Model model, String msg) {
        model.addAttribute("commentErrorMessage", msg);
    }

    private void addCommentSuccessMessage(Model model, String msg) {
        model.addAttribute("commentSuccessMessage", msg);
    }

    private void addRatingErrorMessage(Model model, String msg) {
        model.addAttribute("ratingErrorMessage", msg);
    }

    private void addRatingSuccessMessage(Model model, String msg) {
        model.addAttribute("ratingSuccessMessage", msg);
    }

    private void addAvgStars(Model model, int rating) {
        List<String> avgStars= new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            if (i < rating) {
                avgStars.add("full");
            }
            else {
                avgStars.add("empty");
            }
        }
        model.addAttribute("avgStars", avgStars);
    }

    private void updateGameField(Model model) {
        Field<Color> matrix = game.getSudokuMatrix();
        IntStream.range(0, fieldSize).forEach(i -> {
            IntStream.range(0, fieldSize).forEach(j -> {
                Cell<Color> item = matrix.getCell(i,j);
                if (item instanceof Guess) {
                    gameField[i][j] = item.getValue() != null ? item.getValue().toString() + " setable": "setable";
                }
                else {
                    gameField[i][j] = item.getValue() != null ? item.getValue().toString() : "";
                }
            });
        });
        model.addAttribute("rows", gameField);
        model.addAttribute("scores", scoreService.getTopScores(gameName));
        try {
            int avgRating = ratingService.getAverageRating(gameName);
            model.addAttribute("avgRating", avgRating);
            addAvgStars(model, avgRating);
            if (userController.isLogged()) {
                int userRating = ratingService.getRating(gameName, userController.getLoggedUser());
                model.addAttribute("userRating", userRating);
            }
        } catch (RatingException e) {
            model.addAttribute("notRated", true);
        }
        try {
            model.addAttribute("comments", commentService.getComments(gameName));
        } catch (CommentException e) {
            model.addAttribute("nocommented", true);
        }
        System.out.println(color != null ? color.getValue(): "");
        model.addAttribute("currentColor", color != null ? color.toString() : "");
        model.addAttribute("setGuess", setGuess);
        model.addAttribute("deleteGuess", deleteGuess);
    }
}
