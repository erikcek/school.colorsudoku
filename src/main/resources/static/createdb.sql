drop table if exists score;
drop table if exists comment;
drop table if exists rating;

create table score (
                       player varchar(250) not null ,
                       game varchar(250) not null ,
                       points int not null,
                       playedon timestamp not null,
                       primary key (player, playedon, game)
);

create table comment (
                         player varchar(250) not null ,
                         game varchar(250) not null ,
                         comment varchar(1500) not null,
                         commentedon timestamp not null,
                         primary key (player, game, commentedon)
);

create table rating (
                        player varchar(250) not null,
                        game varchar(250) not null,
                        rating int not null,
                        ratedon timestamp not null,
                        primary key (player, game)
)